package main

import (
	"bytes"
	"fmt"
	"math/rand"
	"strconv"
	"time"
)

type Exchange_orders_response struct {
	Data map[string]interface{} `json:"data"`
}

func getOrderedSharesCount(org_id int) int { //TODO
	query := "SELECT initial_shares_count FROM exchange_orders WHERE organization_id=" + strconv.Itoa(org_id) + " and " + " kind='buy' and state!='discarded';"

	post := Exchange_ipos{}
	db.Where("organization_id = ?", org_id).First(&post)
	rows, err := db.Raw(query).Rows()
	defer rows.Close()
	if err != nil {
		//		fmt.Println("Error in db.Raw")
		panic(err)
	}
	sum := 0
	initial_shares_count := 0
	for rows.Next() {
		rows.Scan(&initial_shares_count)
		sum = sum + initial_shares_count
	}
	if post.Published_shares_count < sum {
		sum = post.Published_shares_count
	}
	return sum
}

func getRandomOwner() (int, string) {
	/*
		var (
			Id int
			Type string
		)
			rows, _ := db.Raw("SELECT * FROM shareholders ORDER BY RANDOM() LIMIT 1").Rows()
			defer rows.Close()
			for rows.Next() {
				rows.Scan(&Id, &Type)
			}
	*/
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	types := []string{
		"organization",
		"person",
	}
	return r.Intn(5) + 1, types[r.Intn(len(types))]
}

func getShareholdersByOrganizationId(organization_id int) (int, string) { //TODO
	SH := Shareholders{}
	db.First(&SH).Where("organization_id=", organization_id)
	return SH.Id, SH.Type
}

func createShares(organization_id int, published_shares_count int) {
	owner_id, _ := getShareholdersByOrganizationId(organization_id)

	str := "INSERT INTO exchange_shares (trade_id, owner_id, owner_type, organization_id, state, created_at, updated_at) VALUES"
	//values := "\n(" + strconv.Itoa(0) + ", " + strconv.Itoa(owner_id) + ", 'organization', " + strconv.Itoa(organization_id) + ",  'blocked', " + `'` + time1 + `'` + ", " + `'` + time1 + `'` + ")"
	values := "\n(" + strconv.Itoa(0) + ", " + strconv.Itoa(owner_id) + ", 'organization', " + strconv.Itoa(organization_id) + ",  'blocked', " + `'`
	var buffer bytes.Buffer
	buffer.WriteString(str)
	for i := 0; i < published_shares_count-1; i++ {
		buffer.WriteString(values)
		temp := time.Now().Format("2006-01-02 15:04:05.999999999") + `'` + ", " + `'` + time.Now().Format("2006-01-02 15:04:05.999999999") + `'` + ")"
		buffer.WriteString(temp)
		buffer.WriteString(",")
	}
	buffer.WriteString(values)

	temp := time.Now().Format("2006-01-02 15:04:05.999999999") + `'` + ", " + `'` + time.Now().Format("2006-01-02 15:04:05.999999999") + `'` + ")"
	buffer.WriteString(temp)
	buffer.WriteString(";")
	db.Exec(buffer.String())
}

func changeShares(post Exchange_trades, state string, trade_id int) {

	query := `UPDATE exchange_shares as a SET trade_id=` + strconv.Itoa(trade_id) + ", owner_id=" + strconv.Itoa(post.Buyer_id) + ", owner_type= '" + post.Buyer_type + "' , state='" + state + "'" +
		` FROM (
		SELECT id
		FROM exchange_shares
		Limit ` + strconv.Itoa(post.Shares_count) +
		`) b
		WHERE b.id=a.id and owner_id=` + strconv.Itoa(post.Seller_id) + " and organization_id=" + strconv.Itoa(post.Organization_id) + ";"
	fmt.Println(query)
	db.Exec(query)

}

func postIpoOrder(post Exchange_ipos) {
	owner_id, _ := getShareholdersByOrganizationId(post.Organization_id)
	Order := Exchange_orders{
		State:                "started", //started, completed, discarded
		Kind:                 "sale",
		Shares_count:         post.Published_shares_count, //потом меняется
		Price_per_share:      post.Price_per_share,
		Organization_id:      post.Organization_id,
		Owner_type:           "organization",
		Owner_id:             owner_id,
		Created_at:           time.Now(),
		Updated_at:           time.Now(),
		Ipo:                  true,
		Auto_trade:           false,
		Initial_shares_count: post.Published_shares_count, //скорее всего не меняется
	}
	db.Create(&Order)
}

func iposResponse(post Exchange_ipos) Exchange_ipos_response {
	var days, hours, minutes, seconds float64
	if post.State != "complete" {
		tt := -time.Now().Sub(post.End_date)
		days = float64(int(tt.Hours() / 24))
		hours = float64(int(tt.Hours() - days*24))
		minutes = float64(int(tt.Minutes() - days*24*60 - hours*60))
		seconds = float64(int(tt.Seconds() - days*24*60*60 - hours*60*60 - minutes*60))
	} else {
		days, hours, minutes, seconds = 0.0, 0.0, 0.0, 0.0
	}

	time_till_end := map[string]float64{
		"d": days,
		"h": hours,
		"m": minutes,
		"s": seconds,
	}
	resp := Exchange_ipos_response{
		Price_per_share:        post.Price_per_share,
		Total_shares_count:     post.Total_shares_count,
		Published_shares_count: post.Published_shares_count,
		Ordered_shares_count:   getOrderedSharesCount(post.Organization_id),
		Cut_off_price:          post.Cut_off_price,
		Earnings:               post.Earnings,
		Time_till_end:          time_till_end,
		State:                  post.State,
		Ticker:                 post.Ticker,
		End_date:               post.End_date.String()[0:10],
	}
	return resp
}

func organizationResponse(post Organizations) interface{} {

	resp := map[string]interface{}{
		"id": post.ID,
		"logo": map[string]interface{}{
			"url":       nil,
			"thumb_url": nil,
		},
		"short_name":   post.Short_name,
		"section_name": "Госвласть",
	}
	return resp

}

func currentMarketPricePerShare(orgid int) float64 {
	return 0.0 //TODO
}

func yesterdayMarketPricePerShare(orgid int) float64 {
	return 0.0 //TODO
}

func sharePriceChange(v1, v2 float64) float64 {
	if v1 == 0 {
		return 0.0
	} else {
		return (v2 - v1) / v1 * 100
	}
}

func ordersResponse(post Exchange_orders, order_id int) interface{} {

	cmpps := currentMarketPricePerShare(post.Organization_id)
	ympps := yesterdayMarketPricePerShare(post.Organization_id)
	spc := sharePriceChange(ympps, cmpps)
	Organization_market_info := map[string]float64{ //TODO
		"current_market_price_per_share":   cmpps,
		"yesterday_market_price_per_share": ympps,
		"current_market_price":             cmpps * float64(post.Initial_shares_count), //TODO Уточнить! может там post.Shares_count
		"share_price_change":               spc,
		"current_net_price":                0.0,
		"current_net_price_per_share":      0.0,
		"rs_bs": 0.0,
	}
	//если не будет работать, то
	//скорее всего придется расписывать интерфейсы и функции, их возвращающие
	temp := Exchange_ipos{}
	db.Where("organization_id = ?", post.Organization_id).First(&temp)
	Ipo := iposResponse(temp)

	temp2 := Organizations{}
	db.Where("id = ?", post.Organization_id).First(&temp2)
	Organization := organizationResponse(temp2)

	Data := map[string]interface{}{
		"id":                       order_id,
		"shares_count":             post.Shares_count,
		"price_per_share":          post.Price_per_share,
		"kind":                     post.Kind,
		"created_at":               post.Created_at,
		"state":                    post.State,
		"initial_shares_count":     post.Initial_shares_count,
		"packet_price":             float64(post.Shares_count) * post.Price_per_share,
		"organization_market_info": Organization_market_info,
		"ipo":          Ipo,
		"organization": Organization,
	}
	resp := map[string]map[string]interface{}{
		"data": Data,
	}
	return resp
}

func tradesResponse(post Exchange_trades, id_of_trade int) interface{} {
	Data := map[string]interface{}{
		"id":              id_of_trade,
		"organization_id": post.Organization_id,
		"shares_count":    post.Shares_count,
		"price_per_share": post.Price_per_share,
		"total_price":     float64(post.Shares_count) * post.Price_per_share,
		"state":           "completed",
	}
	resp := map[string]map[string]interface{}{
		"data": Data,
	}
	return resp
}

func publishedSharesCount(organization_id int) int {
	return 0
}

func organizationsResponse(organization_id int) interface{} {

	cmpps := currentMarketPricePerShare(organization_id)
	ympps := yesterdayMarketPricePerShare(organization_id)
	spc := sharePriceChange(ympps, cmpps)
	psc := publishedSharesCount(organization_id)
	Organization_market_info := map[string]float64{ //TODO
		"current_market_price_per_share":   cmpps,
		"yesterday_market_price_per_share": ympps,
		"current_market_price":             cmpps * float64(psc), //TODO
		"share_price_change":               spc,
		"current_net_price":                0.0,
		"current_net_price_per_share":      0.0,
		"rs_bs": 0.0,
	}

	temp := Exchange_ipos{}
	db.Where("organization_id = ?", organization_id).First(&temp)
	Ipo := iposResponse(temp)

	temp2 := Organizations{}
	db.Where("id = ?", organization_id).First(&temp2)
	Organization := organizationResponse(temp2)

	Data := map[string]interface{}{
		"organization_market_info": Organization_market_info,
		"ipo":          Ipo,
		"organization": Organization,
	}

	return Data

}
