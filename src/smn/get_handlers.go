package main

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	//"github.com/jinzhu/gorm"
	_ "github.com/lib/pq"
	//"log"
	"net/http"
	//	"github.com/metakeule/fmtdate"
	//"io/ioutil"
	"reflect"
	"strconv"
	//"time"
)

func GetOrganizations(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-type", "application/json")

	UrlQuery := r.URL.Query()
	ipo := UrlQuery.Get("ipo")
	var query string
	if ipo == "" {
		query = "select organization_id from exchange_ipos;"
	} else {
		query = "select organization_id from exchange_ipos where state= '" + ipo + "';"
	}
	rows, err := db.Raw(query).Rows()
	if err != nil {
		fmt.Println("Error in select")
		panic(err)
	}
	defer rows.Close()
	var id int
	ids := make([]int, 0)
	for rows.Next() {
		rows.Scan(&id)
		ids = append(ids, id)
	}

	Data := make([]interface{}, 0)
	for i := 0; i < len(ids); i++ {
		//fmt.Println(organizationsResponse(ids[i]))
		Data = append(Data, organizationsResponse(ids[i]))
	}

	Total := len(ids)
	Meta := map[string]interface{}{
		"total": Total,
	}

	//response

	resp := &Organizations_response{
		Meta: Meta,
		Data: Data,
	}
	/*
		resp:=map[string]interface{}{
			"meta": Meta,
			"data": Data,
		}*/
	res1B, _ := json.Marshal(resp)
	fmt.Fprintf(w, "%s", string(res1B))

}

func GetOrderId(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-type", "application/json")
	vars := mux.Vars(r)
	id := vars["id"]
	post := Exchange_orders{}
	db.Where("id = ?", id).First(&post)
	id_int, _ := strconv.Atoi(id)
	resp := ordersResponse(post, id_int)
	res1B, _ := json.Marshal(resp)
	fmt.Fprintf(w, "%s", string(res1B))
}

func GetIpo(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-type", "application/json")
	//параметры запроса
	vars := mux.Vars(r)
	id := vars["organization_id"]
	post := Exchange_ipos{}
	temp, _ := (strconv.ParseInt(id, 0, 0))
	int_id := int(temp)
	db.Where("organization_id = ?", int_id).First(&post)
	resp := iposResponse(post)
	res1B, _ := json.Marshal(resp)
	fmt.Fprintf(w, "%s", string(res1B))
}

func GetOrderByOrgId(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-type", "application/json")
	UrlQuery := r.URL.Query()
	vars := mux.Vars(r)

	id := vars["organization_id"]
	state := UrlQuery.Get("state")
	ipo := UrlQuery.Get("ipo")
	if reflect.TypeOf(id).String() != "int" || reflect.TypeOf(state).String() != "string" || reflect.TypeOf(ipo).String() != "string" {
		fmt.Fprintf(w, "%s", "Ошибка параметров запроса")
		return
	}

}

func GetTradeIdByOrgId(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-type", "application/json")
	//параметры запроса
	vars := mux.Vars(r)
	//org_id_String := vars["organization_id"]
	id_String := vars["id"]
	id_int, _ := strconv.Atoi(id_String)
	post := Exchange_trades{}
	db.Where("id = ?", id_int).First(&post)
	resp := tradesResponse(post, id_int)
	res1B, _ := json.Marshal(resp)
	fmt.Fprintf(w, "%s", string(res1B))

}
