// smn project main.go
package main

import (
	"database/sql"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	_ "github.com/lib/pq"
	"log"
	"net/http"
)

type org struct {
	Id   int
	Name string
}

var (
	db     gorm.DB
	db_sql *sql.DB
	err    error
)

func main() {

	db_sql, _ = sql.Open("postgres", "user=postgres password=password dbname=postgres sslmode=disable")
	defer db_sql.Close()
	db, err = gorm.Open("postgres", "user=postgres password=password dbname=postgres sslmode=disable")
	if err != nil {
		fmt.Println("Error in connect DB(gorm)")
		log.Fatal(err)
	}

	db.SingularTable(true)
	r := mux.NewRouter()

	//r.HandleFunc("/v1/exchange/orders(.{format:[a-z]+})", GetOrders ).Methods("GET")	//TODO
	//r.HandleFunc("/v1/exchange/{organization_id:[0-9]+}/orders", GetOrderByOrgId).Methods("GET") //TODO
	r.HandleFunc("/v1/exchange/orders/{id:[0-9]+}", GetOrderId).Methods("GET")
	r.HandleFunc("/v1/exchange/orders/{id:[0-9]+}/discard", PostOrderDiscard).Methods("POST")

	//r.HandleFunc("/v1/exchange/{organization_id:[0-9]+}/trades", GetTradeByOrgId).Methods("GET") //TODO
	r.HandleFunc("/v1/exchange/{organization_id:[0-9]+}/trades/{id:[0-9]+}", GetTradeIdByOrgId).Methods("GET")
	r.HandleFunc("/v1/exchange/{organization_id:[0-9]+}/trades", PostTrade).Methods("POST")

	r.HandleFunc("/v1/exchange/{organization_id:[0-9]+}/orders", PostOrder).Methods("POST")

	r.HandleFunc("/v1/exchange/{organization_id:[0-9]+}/ipo", PostIpo).Methods("POST")
	r.HandleFunc("/v1/exchange/{organization_id:[0-9]+}/ipo/complete", PostIpoComplete).Methods("POST")
	r.HandleFunc("/v1/exchange/{organization_id:[0-9]+}/ipo", GetIpo).Methods("GET")

	r.HandleFunc("/v1/exchange/organizations", GetOrganizations).Methods("GET")
	//r.HandleFunc("/v1/exchange/organizations/{id:[0-9]+}", GetOrganizationById).Methods("GET")

	http.Handle("/", r)
	fmt.Println("it works!")
	http.ListenAndServe(":80", nil)
	defer db.Close()

}
