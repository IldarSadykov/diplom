package main

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
	"github.com/metakeule/fmtdate"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"time"
)

//IPO
func PostIpo(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-type", "application/json")
	vars := mux.Vars(r) //параметры запроса
	str_id := vars["organization_id"]
	result, err := ioutil.ReadAll(r.Body)
	r.Body.Close()
	if err != nil {
		log.Fatal(err)
		fmt.Println("err in ioutil.ReadAll")
	}
	body := map[string]interface{}{
		"price_per_share":        0.0,
		"total_shares_count":     0,
		"published_shares_count": 0,
		"ticker":                 "",
		"end_date":               "",
	}
	f := map[string]map[string]interface{}{
		"data": body,
	}
	err = json.Unmarshal(result, &f)
	if err != nil {
		panic(err)
	}

	month := int(time.Now().Month())
	temp, _ := (strconv.ParseInt(str_id, 0, 0))
	int_id := int(temp)

	//time

	time_string := f["data"]["end_date"].(string)
	time_temp, err := fmtdate.Parse("2006-01-02", time_string)
	if err != nil {
		log.Fatal(err)
		fmt.Println("err in fmtdate.Parse")
	}

	post := Exchange_ipos{
		Price_per_share:        f["data"]["price_per_share"].(float64),
		Total_shares_count:     int(f["data"]["total_shares_count"].(float64)), //f["data"]["total_shares_count"].(int),
		Month:                  month,                                          //зачем он вообще нужен?
		Organization_id:        int_id,
		Created_at:             time.Now(),
		Updated_at:             time.Now(),
		Published_shares_count: int(f["data"]["published_shares_count"].(float64)),
		End_date:               time_temp,
		State:                  "active",
		Earnings:               0.0,
		Cut_off_price:          f["data"]["price_per_share"].(float64),
		Ticker:                 f["data"]["ticker"].(string),
	}

	err = db.Save(&post).Error
	if err != nil {
		panic(err)
	}

	//создаем акции - время создания акций одинаково
	createShares(post.Organization_id, post.Published_shares_count)

	//сохраняем заказ
	postIpoOrder(post)

	//response

	resp := iposResponse(post)
	res1B, _ := json.Marshal(resp)
	fmt.Fprintf(w, "%s", string(res1B))

}

func PostIpoComplete(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-type", "application/json")
	vars := mux.Vars(r) //параметры запроса
	id := vars["organization_id"]
	id_int, _ := strconv.Atoi(id)
	//обновляем ipos
	post := Exchange_ipos{}
	db.Where("organization_id = ?", id).First(&post)
	if post.Organization_id != 0 {
		post.State = "complete"
	}
	query := "UPDATE exchange_ipos SET state='complete' WHERE organization_id=" + id + ";"
	db.Exec(query)
	ordered_shares_count := 0
	order := Exchange_orders{}

	var (
		trade_sc    int
		id_of_order int
	)
	for ordered_shares_count < post.Published_shares_count {

		query := "SELECT * FROM exchange_orders WHERE ipo=false and organization_id = " + strconv.Itoa(id_int) + " and kind='buy' and state='started' and shares_count>0 ORDER BY price_per_share desc LIMIT 1;"

		rows, err := db.Raw(query).Rows()
		defer rows.Close()
		if err != nil {
			//fmt.Println("Error in db.Raw")
			panic(err)
		}
		for rows.Next() {
			rows.Scan(&order.Id, &order.State, &order.Kind, &order.Shares_count, &order.Price_per_share, &order.Organization_id, &order.Owner_type, &order.Owner_id, &order.Created_at, &order.Updated_at, &order.Ipo, &order.Auto_trade, &order.Initial_shares_count)
			break
		}

		if ordered_shares_count+order.Shares_count > post.Published_shares_count {

			trade_sc = post.Published_shares_count - ordered_shares_count
			//order.Shares_count = order.Shares_count - trade_sc
			ordered_shares_count = post.Published_shares_count
			new_order_sc := order.Shares_count - trade_sc
			//trade_sc = new_order_sc
			db.First(&order, order.Id).Update("shares_count", new_order_sc)

		} else {
			trade_sc = order.Shares_count
			ordered_shares_count = ordered_shares_count + order.Shares_count
			db.First(&order, order.Id).Update("shares_count", 0).Update("state", "complete")

		}

		owner_id, owner_type := getShareholdersByOrganizationId(id_int)
		query = "SELECT id FROM exchange_orders WHERE organization_id=" + strconv.Itoa(id_int) + " and ipo=true and kind='sale';"
		rows, err = db.Raw(query).Rows()
		defer rows.Close()
		if err != nil {
			//		fmt.Println("Error in db.Raw")
			panic(err)
		}
		for rows.Next() {
			rows.Scan(&id_of_order)
		}

		//постим trade
		post_trade := Exchange_trades{
			Buy_order_id:    order.Id,
			Sale_order_id:   id_of_order,
			Shares_count:    trade_sc,
			Price_per_share: order.Price_per_share,
			Organization_id: id_int,
			Seller_type:     owner_type,
			Seller_id:       owner_id,
			Buyer_type:      order.Owner_type,
			Buyer_id:        order.Owner_id,
			Created_at:      time.Now(),
			Updated_at:      time.Now(),
			Ipo:             false,
			State:           "completed",
		}
		post.Earnings += post_trade.Price_per_share * float64(post_trade.Shares_count)
		/*
			fmt.Println("post_trade = ", post_trade)
			fmt.Println("post.Earnings = ", post.Earnings)
		*/
		err = db.Save(&post_trade).Error
		if err != nil {
			panic(err)
		}

		//id посленего trade
		query = "SELECT currval('exchange_trades_id_seq')"
		rows, err = db.Raw(query).Rows()
		defer rows.Close()
		if err != nil {
			//fmt.Println("Error in db.Raw")
			panic(err)
		}
		var id int
		for rows.Next() {
			rows.Scan(&id)
		}
		id_of_trade := id
		//меняем купленные акции - назначаем владельца, состояние, сделку
		changeShares(post_trade, "active", id_of_trade)

	}
	query = "UPDATE exchange_ipos SET  earnings=" + strconv.FormatFloat(post.Earnings, 'f', 2, 64) + " WHERE organization_id=" + strconv.Itoa(id_int) + ";"
	db.Exec(query)
	query = "UPDATE exchange_orders SET  shares_count=0 WHERE organization_id=" + strconv.Itoa(id_int) + " and kind='sale' and ipo=true;"
	db.Exec(query)
	resp := iposResponse(post)
	res1B, _ := json.Marshal(resp)
	fmt.Fprintf(w, "%s", string(res1B))

}

//Order
//TODO
func PostOrder(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-type", "application/json")
	vars := mux.Vars(r) //параметры запроса
	str_id := vars["organization_id"]
	result, err := ioutil.ReadAll(r.Body)
	r.Body.Close()
	if err != nil {
		log.Fatal(err)
		fmt.Println("err in ioutil.ReadAll")
	}

	body := map[string]interface{}{
		"shares_count":    0,
		"price_per_share": 0,
		"kind":            "",
	}
	f := map[string]map[string]interface{}{
		"data": body,
	}

	err = json.Unmarshal(result, &f)
	if err != nil {
		panic(err)
	}

	temp, _ := (strconv.ParseInt(str_id, 0, 0))
	int_id := int(temp)
	owner_id, owner_type := getRandomOwner()

	post := Exchange_orders{
		State:                "started",
		Kind:                 f["data"]["kind"].(string),
		Shares_count:         int(f["data"]["shares_count"].(float64)),
		Price_per_share:      f["data"]["price_per_share"].(float64),
		Organization_id:      int_id,
		Owner_type:           owner_type, //TODO
		Owner_id:             owner_id,   //TODO
		Created_at:           time.Now(),
		Updated_at:           time.Now(),
		Ipo:                  false,
		Auto_trade:           true,
		Initial_shares_count: int(f["data"]["shares_count"].(float64)),
	}

	//post order

	err = db.Save(&post).Error
	if err != nil {
		panic(err)
	}

	var id int

	query := "SELECT currval('exchange_orders_id_seq')"
	rows, err := db.Raw(query).Rows()
	defer rows.Close()
	if err != nil {
		//fmt.Println("Error in db.Raw")
		panic(err)
	}
	for rows.Next() {
		rows.Scan(&id)
	}
	id_of_order := id
	//потом еще нужна проверка - чтобы у продавца было достаточно акций для продажи
	//блокируем акции продавца

	if post.Kind == "sale" { //TODO
		//select id from exchange_shares where owner_id=1 and organization_id=2 Limit 20;
		//fmt.Println("post.Owner_id=", post.Owner_id, " str_id=", str_id, " strconv.Itoa(post.Shares_count)=", strconv.Itoa(post.Shares_count))
		rows, err = db.Raw("select id from exchange_shares where owner_id=" + strconv.Itoa(post.Owner_id) + " and organization_id=" + str_id + " Limit " + strconv.Itoa(post.Shares_count) + ";").Rows()
		for rows.Next() {
			rows.Scan(&id)

			query = "UPDATE exchange_shares SET state='blocked' WHERE id=" + strconv.Itoa(id) + ";"
			db.Exec(query)
		}
	}

	//create response

	resp := ordersResponse(post, id_of_order)
	res1B, _ := json.Marshal(resp)
	fmt.Fprintf(w, "%s", string(res1B))

}

func PostOrderDiscard(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-type", "application/json")
	vars := mux.Vars(r) //параметры запроса
	id := vars["id"]
	post := Exchange_orders{}
	db.Where("id = ?", id).First(&post)
	if post.Shares_count != 0 {
		post.State = "discarded"
	}
	query := "UPDATE exchange_orders SET state='discarded' WHERE id=" + id + ";"
	db.Exec(query)
	id_int, _ := strconv.Atoi(id)
	resp := ordersResponse(post, id_int)
	res1B, _ := json.Marshal(resp)
	fmt.Fprintf(w, "%s", string(res1B))
}

//TODO - немного осталось - про акции
func PostTrade(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-type", "application/json")
	vars := mux.Vars(r) //параметры запроса
	str_id := vars["organization_id"]
	result, err := ioutil.ReadAll(r.Body)
	r.Body.Close()
	if err != nil {
		log.Fatal(err)
		fmt.Println("err in ioutil.ReadAll")
	}

	f := map[string]interface{}{
		"order_id":     0,
		"shares_count": 0,
	}
	err = json.Unmarshal(result, &f)
	if err != nil {
		panic(err)
	}
	int_id, _ := strconv.Atoi(str_id)
	order := Exchange_orders{}
	db.Where("id = ?", int(f["order_id"].(float64))).First(&order)
	var (
		sc       int //shares_count
		kind     string
		state    string
		trade_sc int
	)

	if order.Kind == "sale" { //покупаем акции
		kind = "buy"
		if int(f["shares_count"].(float64)) >= order.Shares_count { //если покупаем больше, чем продают

			state = "started"
			//обновляем order - state "completed", order.shares_count:=0
			query := "UPDATE exchange_orders SET state='completed', shares_count=0  WHERE id=" + strconv.Itoa(int(f["order_id"].(float64))) + ";"
			db.Exec(query)
			sc = int(f["shares_count"].(float64)) - order.Shares_count

		} else { //покупаем меньше, чем продают

			//			обновляем order - order.shares_count=order.Shares_count-int(f["shares_count"].(float64))
			state = "completed"
			new_order_sc := order.Shares_count - int(f["shares_count"].(float64))
			query := "UPDATE exchange_orders SET shares_count=" + strconv.Itoa(new_order_sc) + " WHERE id=" + strconv.Itoa(int(f["order_id"].(float64))) + ";"
			db.Exec(query)
			sc = order.Shares_count - new_order_sc
		}

	} else { //order.Kind=="buy",
		kind = "sale"
		if int(f["shares_count"].(float64)) >= order.Shares_count { //если продаем больше, чем покупают

			state = "started"
			//			обновляем order - state "completed", order.shares_count:=0
			query := "UPDATE exchange_orders SET state='completed', shares_count=0  WHERE id=" + strconv.Itoa(int(f["order_id"].(float64))) + ";"
			db.Exec(query)
			sc = int(f["shares_count"].(float64)) - order.Shares_count
		} else { //продаем меньше, чем покупают

			//			обновляем order - order.shares_count=order.Shares_count-int(f["data"]["shares_count"].(float64))
			new_order_sc := order.Shares_count - int(f["shares_count"].(float64))
			query := "UPDATE exchange_orders SET shares_count=" + strconv.Itoa(new_order_sc) + " WHERE id=" + strconv.Itoa(int(f["order_id"].(float64))) + ";"
			db.Exec(query)
			state = "completed"
			sc = order.Shares_count - new_order_sc
		}
	}
	if sc >= order.Shares_count {
		trade_sc = sc
	} else {
		trade_sc = order.Shares_count
	}
	owner_id, owner_type := getRandomOwner()

	new_order := Exchange_orders{
		State:                state,
		Kind:                 kind,
		Shares_count:         sc,
		Price_per_share:      order.Price_per_share,
		Organization_id:      int_id,
		Owner_type:           owner_type,
		Owner_id:             owner_id,
		Created_at:           time.Now(),
		Updated_at:           time.Now(),
		Ipo:                  false,
		Auto_trade:           true,
		Initial_shares_count: order.Initial_shares_count,
	}
	//постим new_order
	err = db.Save(&new_order).Error
	if err != nil {
		panic(err)
	}

	//находим id нового заказа
	query := "SELECT currval('exchange_orders_id_seq')"
	rows, err := db.Raw(query).Rows()
	defer rows.Close()
	if err != nil {
		//fmt.Println("Error in db.Raw")
		panic(err)
	}
	var id int
	for rows.Next() {
		rows.Scan(&id)
	}
	id_of_order := id

	//постим trade с параметрами:
	var (
		seller_type, buyer_type                          string
		buy_order_id, sale_order_id, buyer_id, seller_id int
	)
	if kind == "buy" {
		buy_order_id = id_of_order
		buyer_type = new_order.Owner_type
		buyer_id = new_order.Owner_id

		sale_order_id = int(f["order_id"].(float64))
		seller_type = order.Owner_type
		seller_id = order.Owner_id

	} else {
		buy_order_id = int(f["order_id"].(float64))
		buyer_type = order.Owner_type
		buyer_id = order.Owner_id

		sale_order_id = id_of_order
		seller_type = new_order.Owner_type
		seller_id = new_order.Owner_id
	}

	post_trade := Exchange_trades{
		Buy_order_id:    buy_order_id,
		Sale_order_id:   sale_order_id,
		Shares_count:    trade_sc,
		Price_per_share: order.Price_per_share,
		Organization_id: int_id,
		Seller_type:     seller_type,
		Seller_id:       seller_id,
		Buyer_type:      buyer_type,
		Buyer_id:        buyer_id,
		Created_at:      time.Now(),
		Updated_at:      time.Now(),
		Ipo:             false,
		State:           "completed",
	}

	err = db.Save(&post_trade).Error
	if err != nil {
		panic(err)
	}

	//id посленего trade
	query = "SELECT currval('exchange_trades_id_seq')"
	rows, err = db.Raw(query).Rows()
	defer rows.Close()
	if err != nil {
		//fmt.Println("Error in db.Raw")
		panic(err)
	}
	//var id int
	for rows.Next() {
		rows.Scan(&id)
	}
	id_of_trade := id
	//TODO
	//меняем владельца акций в таблице shares!	--подумать, как это сделать ( +смена состояния - разблокировка акций, чтобы покупатель мог их потом продать)
	//еще нужно взять id последнего трейда  и засунуть в поле trade_id в shares
	changeShares(post_trade, "active", id_of_trade)

	//response
	resp := tradesResponse(post_trade, id_of_trade)
	res1B, _ := json.Marshal(resp)
	fmt.Fprintf(w, "%s", string(res1B))
}
