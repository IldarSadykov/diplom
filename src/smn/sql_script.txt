--DELETE ALL TABLES

DROP TABLE exchange_trades;
DROP TABLE exchange_orders;
DROP TABLE exchange_shares;
DROP TABLE exchange_ipos;
DROP TABLE shareholders;
DROP TABLE organizations;


-- Table: organizations

CREATE TABLE organizations
(
  id serial,
  organization_name character varying(45) NOT NULL,
  short_name character varying(45) NOT NULL,
  CONSTRAINT organizations_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE organizations
  OWNER TO postgres;

  
-- Table: exchange_ipos


CREATE TABLE exchange_ipos
(
  price_per_share real,
  total_shares_count integer,
  month integer,
  organization_id integer UNIQUE,
  created_at timestamp without time zone NOT NULL,
  updated_at timestamp without time zone NOT NULL,
  published_shares_count integer,
  end_date timestamp without time zone,
  state character varying(30),
  earnings real DEFAULT 0.0,
  cut_off_price real DEFAULT 0.0,
  ticker character varying(30) NOT NULL,
  CONSTRAINT foreign_key01 FOREIGN KEY (organization_id)
      REFERENCES organizations (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE exchange_ipos
  OWNER TO postgres;


  
  
-- Table: shareholders

CREATE TABLE shareholders
(
  id serial NOT NULL,
  type character varying(45),
  organization_id int,
  CONSTRAINT shareholders_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE shareholders
  OWNER TO postgres;

  

-- Table: exchange_shares

CREATE TABLE exchange_shares
(
  id serial,
  trade_id integer NOT NULL,
  owner_id integer,
  owner_type character varying(45),
  organization_id integer,
  state character varying(45),
  created_at timestamp without time zone NOT NULL,
  updated_at timestamp without time zone NOT NULL,
  CONSTRAINT foreign_key01 FOREIGN KEY (organization_id)
      REFERENCES organizations (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT foreign_key02
    FOREIGN KEY (owner_id)
    REFERENCES public.shareholders(id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE exchange_shares
  OWNER TO postgres;

  
-- Table: exchange_orders


CREATE TABLE exchange_orders
(
  id serial,
  state character varying(45) NOT NULL,
  kind character varying(45) NOT NULL,
  shares_count integer NOT NULL,
  price_per_share real NOT NULL,
  organization_id integer NOT NULL,
  owner_type character varying(45) NOT NULL,
  owner_id integer NOT NULL,
  created_at timestamp without time zone NOT NULL,
  updated_at timestamp without time zone NOT NULL,
  ipo boolean DEFAULT false,
  auto_trade boolean NOT NULL DEFAULT true,
  initial_shares_count integer,
  CONSTRAINT exchange_orders_pkey PRIMARY KEY (id),
  CONSTRAINT fk_exchange_orders_shareholders1 FOREIGN KEY (owner_id)
      REFERENCES shareholders (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT
)
WITH (
  OIDS=FALSE
);
ALTER TABLE exchange_orders
  OWNER TO postgres;

  
-- Table: exchange_trades


CREATE TABLE exchange_trades
(
  id serial,
  buy_order_id integer NOT NULL,
  sale_order_id integer NOT NULL,
  shares_count integer,
  price_per_share real,
  organization_id integer NOT NULL,
  seller_type character varying(45) NOT NULL,
  seller_id integer NOT NULL,
  buyer_type character varying(45) NOT NULL,
  buyer_id integer NOT NULL,
  created_at timestamp without time zone NOT NULL,
  updated_at timestamp without time zone NOT NULL,
  ipo boolean DEFAULT false,
  state character varying(45),  
  CONSTRAINT exchange_trades_pkey PRIMARY KEY (id),
  CONSTRAINT fk_exchange_trades_exchange_orders1 FOREIGN KEY (buy_order_id)
      REFERENCES exchange_orders (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT,
  CONSTRAINT fk_exchange_trades_exchange_orders2 FOREIGN KEY (sale_order_id)
      REFERENCES exchange_orders (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT,
  CONSTRAINT fk_exchange_trades_shareholders1 FOREIGN KEY (seller_id)
      REFERENCES shareholders (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT,
  CONSTRAINT fk_exchange_trades_shareholders2 FOREIGN KEY (buyer_id)
      REFERENCES shareholders (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT
)
WITH (
  OIDS=FALSE
);
ALTER TABLE exchange_trades
  OWNER TO postgres;


--	------------------------
INSERT INTO organizations (organization_name, short_name)
VALUES('Название_организации_1', 'Назв_орг_1');
INSERT INTO organizations (organization_name, short_name)
VALUES('Рога_и_копыта', 'РиК');


INSERT INTO shareholders
VALUES (1, 'organization', 1);
INSERT INTO shareholders
VALUES (2, 'organization', 2);
INSERT INTO shareholders
VALUES (3, 'person', 0);


--INSERT INTO exchange_orders ( state,  kind, shares_count, price_per_share, organization_id, owner_type, owner_id, created_at, updated_at, ipo, auto_trade, initial_shares_count)
--VALUES ('started', 'sell', 20, 10.5, 1, 'organization', '1', 'Jan-08-1999 04:05', 'Jan-09-1999 04:05', true, false, 20);
--Select nextval('exchange_orders_id_seq');
--SELECT currval('exchange_orders_id_seq');

--INSERT INTO exchange_shares (trade_id, owner_id, owner_type, organization_id, state, created_at, updated_at) VALUES
--(0, 1, 'organization', 1, 'blocked', '2015-05-22 12:53:11.637051', '2015-05-22 12:53:11.637051'),

--INSERT INTO exchange_orders
--VALUES (2, 'started', 'sell', 20, 10.5, 1, 'organization', '1', 'Jan-08-1999 04:05', 'Jan-09-1999 04:05', true, false, 20);