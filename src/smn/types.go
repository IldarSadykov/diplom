package main

import (
	"time"
)

type Shareholders struct {
	Id              int `gorm:"primary_key"`
	Type            string
	Organization_id int
}

type Exchange_ipos struct {
	Price_per_share        float64
	Total_shares_count     int
	Month                  int
	Organization_id        int
	Created_at             time.Time `sql:"not null"`
	Updated_at             time.Time `sql:"not null"`
	Published_shares_count int
	End_date               time.Time
	State                  string
	Earnings               float64 `sql:"DEFAULT 0.0"`
	Cut_off_price          float64 `sql:"DEFAULT 0.0"`
	Ticker                 string  `sql:"not null"`
}

type Exchange_orders struct {
	Id                   int       `gorm:"primary_key"`
	State                string    `sql:"not null"`
	Kind                 string    `sql:"not null"`
	Shares_count         int       `sql:"not null"`
	Price_per_share      float64   `sql:"not null"`
	Organization_id      int       `sql:"not null"`
	Owner_type           string    `sql:"not null"`
	Owner_id             int       `sql:"not null"`
	Created_at           time.Time `sql:"not null"`
	Updated_at           time.Time `sql:"not null"`
	Ipo                  bool      `sql:"DEFAULT false"`
	Auto_trade           bool      `sql:"DEFAULT true;not null"`
	Initial_shares_count int
}

type Exchange_shares struct {
	Trade_id        int
	Owner_id        int
	Owner_type      string
	Organization_id int
	State           string
	Created_at      time.Time `sql:"not null"`
	Updated_at      time.Time `sql:"not null"`
}

type Exchange_trades struct {
	Buy_order_id    int `sql:"not null"`
	Sale_order_id   int `sql:"not null"`
	Shares_count    int
	Price_per_share float64
	Organization_id int       `sql:"not null"`
	Seller_type     string    `sql:"not null"`
	Seller_id       int       `sql:"not null"`
	Buyer_type      string    `sql:"not null"`
	Buyer_id        int       `sql:"not null"`
	Created_at      time.Time `sql:"not null"`
	Updated_at      time.Time `sql:"not null"`
	Ipo             bool      `sql:"DEFAULT false"`
	State           string
}

type Organizations struct {
	ID                int
	Organization_name string `sql:"not null"`
	Short_name        string `sql:"not null"`
}

type Exchange_ipos_response struct {
	Price_per_share        float64            `json:"price_per_share"`
	Total_shares_count     int                `json:"total_shares_count"`
	Published_shares_count int                `json:"published_shares_count"`
	Ordered_shares_count   int                `json:"ordered_shares_count"`
	Cut_off_price          float64            `json:"cut_off_price"`
	Earnings               float64            `json:"earnings"`
	State                  string             `json:"state"`
	Time_till_end          map[string]float64 `json:"time_till_end"`
	Ticker                 string             `json:"ticker"`
	End_date               string             `json:"end_date"` //time.Time          `json:"end_date"`
}

type Organizations_response struct {
	Meta interface{}   `json:"meta"`
	Data []interface{} `json:"data"`
}
